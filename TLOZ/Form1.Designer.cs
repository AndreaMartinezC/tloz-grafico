﻿namespace TLOZ
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.marcador = new System.Windows.Forms.Label();
            this.Arbol1 = new System.Windows.Forms.PictureBox();
            this.PBSword = new System.Windows.Forms.PictureBox();
            this.PBEnemigo2 = new System.Windows.Forms.PictureBox();
            this.PBEnemigo1 = new System.Windows.Forms.PictureBox();
            this.PBLink = new System.Windows.Forms.PictureBox();
            this.Arbol2 = new System.Windows.Forms.PictureBox();
            this.Arbol3 = new System.Windows.Forms.PictureBox();
            this.Arbol4 = new System.Windows.Forms.PictureBox();
            this.Arbol5 = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Arbol1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PBSword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PBEnemigo2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PBEnemigo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PBLink)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Arbol2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Arbol3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Arbol4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Arbol5)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(783, 527);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "label1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(824, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "label2";
            this.label2.Visible = false;
            // 
            // marcador
            // 
            this.marcador.AutoSize = true;
            this.marcador.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.marcador.ForeColor = System.Drawing.Color.Firebrick;
            this.marcador.Location = new System.Drawing.Point(31, 506);
            this.marcador.Name = "marcador";
            this.marcador.Size = new System.Drawing.Size(120, 32);
            this.marcador.TabIndex = 6;
            this.marcador.Text = "Puntaje";
            // 
            // Arbol1
            // 
            this.Arbol1.Image = global::TLOZ.Properties.Resources.arbolito1;
            this.Arbol1.Location = new System.Drawing.Point(197, 256);
            this.Arbol1.Name = "Arbol1";
            this.Arbol1.Size = new System.Drawing.Size(30, 60);
            this.Arbol1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Arbol1.TabIndex = 7;
            this.Arbol1.TabStop = false;
            // 
            // PBSword
            // 
            this.PBSword.Image = global::TLOZ.Properties.Resources.SwordIG;
            this.PBSword.Location = new System.Drawing.Point(133, 266);
            this.PBSword.Name = "PBSword";
            this.PBSword.Size = new System.Drawing.Size(20, 40);
            this.PBSword.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PBSword.TabIndex = 5;
            this.PBSword.TabStop = false;
            // 
            // PBEnemigo2
            // 
            this.PBEnemigo2.Image = global::TLOZ.Properties.Resources.Blazing_Bat_3;
            this.PBEnemigo2.Location = new System.Drawing.Point(563, 348);
            this.PBEnemigo2.Name = "PBEnemigo2";
            this.PBEnemigo2.Size = new System.Drawing.Size(60, 30);
            this.PBEnemigo2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PBEnemigo2.TabIndex = 4;
            this.PBEnemigo2.TabStop = false;
            // 
            // PBEnemigo1
            // 
            this.PBEnemigo1.Image = global::TLOZ.Properties.Resources.stalfos;
            this.PBEnemigo1.Location = new System.Drawing.Point(281, 101);
            this.PBEnemigo1.Name = "PBEnemigo1";
            this.PBEnemigo1.Size = new System.Drawing.Size(35, 55);
            this.PBEnemigo1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PBEnemigo1.TabIndex = 3;
            this.PBEnemigo1.TabStop = false;
            // 
            // PBLink
            // 
            this.PBLink.Image = global::TLOZ.Properties.Resources.Link;
            this.PBLink.Location = new System.Drawing.Point(31, 276);
            this.PBLink.Name = "PBLink";
            this.PBLink.Size = new System.Drawing.Size(30, 30);
            this.PBLink.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PBLink.TabIndex = 0;
            this.PBLink.TabStop = false;
            // 
            // Arbol2
            // 
            this.Arbol2.Image = global::TLOZ.Properties.Resources.arbolito1;
            this.Arbol2.Location = new System.Drawing.Point(347, 328);
            this.Arbol2.Name = "Arbol2";
            this.Arbol2.Size = new System.Drawing.Size(30, 60);
            this.Arbol2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Arbol2.TabIndex = 8;
            this.Arbol2.TabStop = false;
            // 
            // Arbol3
            // 
            this.Arbol3.Image = global::TLOZ.Properties.Resources.arbolito1;
            this.Arbol3.Location = new System.Drawing.Point(428, 111);
            this.Arbol3.Name = "Arbol3";
            this.Arbol3.Size = new System.Drawing.Size(30, 60);
            this.Arbol3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Arbol3.TabIndex = 9;
            this.Arbol3.TabStop = false;
            // 
            // Arbol4
            // 
            this.Arbol4.Image = global::TLOZ.Properties.Resources.arbolito1;
            this.Arbol4.Location = new System.Drawing.Point(563, 219);
            this.Arbol4.Name = "Arbol4";
            this.Arbol4.Size = new System.Drawing.Size(30, 60);
            this.Arbol4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Arbol4.TabIndex = 10;
            this.Arbol4.TabStop = false;
            // 
            // Arbol5
            // 
            this.Arbol5.Image = global::TLOZ.Properties.Resources.arbolito1;
            this.Arbol5.Location = new System.Drawing.Point(632, 398);
            this.Arbol5.Name = "Arbol5";
            this.Arbol5.Size = new System.Drawing.Size(30, 60);
            this.Arbol5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Arbol5.TabIndex = 11;
            this.Arbol5.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(31, 474);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(401, 25);
            this.label3.TabIndex = 12;
            this.label3.Text = "Has sobrepasado el limite de la pantalla.";
            this.label3.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(32, 449);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(206, 24);
            this.label4.TabIndex = 13;
            this.label4.Text = "Presione X para atacar.";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(882, 553);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Arbol5);
            this.Controls.Add(this.Arbol4);
            this.Controls.Add(this.Arbol3);
            this.Controls.Add(this.Arbol2);
            this.Controls.Add(this.Arbol1);
            this.Controls.Add(this.marcador);
            this.Controls.Add(this.PBSword);
            this.Controls.Add(this.PBEnemigo2);
            this.Controls.Add(this.PBEnemigo1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.PBLink);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "Form1";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            ((System.ComponentModel.ISupportInitialize)(this.Arbol1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PBSword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PBEnemigo2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PBEnemigo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PBLink)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Arbol2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Arbol3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Arbol4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Arbol5)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox PBLink;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox PBEnemigo1;
        private System.Windows.Forms.PictureBox PBEnemigo2;
        private System.Windows.Forms.PictureBox PBSword;
        private System.Windows.Forms.Label marcador;
        private System.Windows.Forms.PictureBox Arbol1;
        private System.Windows.Forms.PictureBox Arbol2;
        private System.Windows.Forms.PictureBox Arbol3;
        private System.Windows.Forms.PictureBox Arbol4;
        private System.Windows.Forms.PictureBox Arbol5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}

