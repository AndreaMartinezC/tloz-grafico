﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TLOZ
{
    class Obstaculo
    {
        int perro = 0;
        int gato = 0;
        public static int BloquearX(int izq, int top)
        {
            int perro=0;
            //arbol1 a la izq
            if ((izq == 173 && top == 224) || (izq == 173 && top == 194))
            {
                perro = 1;
            }
            //arbol1 a la derecha
           else if ((izq == 113 && top == 224) || (izq == 113 && top == 194))
            {
                perro = 2;
            }
            //arbol2 a la izq
            else if ((izq == 293 && top == 284) || (izq == 293 && top == 254))
            {
                perro = 3;
            }
            //arbol2 a la derecha
            else if ((izq== 233 && top == 284) || (izq == 233 && top == 254))
            {
                perro = 4;
            }
            //arbol3 a la izq
            else if((izq == 353 && top == 104) || (izq == 353 && top == 74))
            {
                perro = 5;
            }
            //arbol3 a la derecha
            else if ((izq == 293 && top == 104) || (izq == 293 && top == 74))
            {
                perro = 6;
            }
            //arbol4 a la izq
            else if ((izq == 443 && top== 164) || (izq == 443 && top == 194))
            {
                perro = 7;
            }
            //arbol4 a la derecha
            else if ((izq == 383 && top == 164) || (izq == 383 && top == 194))
            {
                perro = 8;
            }
            //arbol5 a la izq
            else if ((izq == 503 && top== 314) || (izq== 503 && top == 344))
            {
                perro = 9;
            }
            //arbol5 a la derecha
            else if ((izq == 443 && top == 314) || (izq == 443 && top == 344))
            {
                perro = 10;
            }

                return perro;


        }

        public static int BloquearY(int izq, int top)
        {
            int gato = 0;
            //todos los de up
            if ((izq == 143 && top == 254))
            {
                gato = 1;
            }
            else if ((izq == 263 && top == 314))
            {
                gato = 2;
            }
            else if ((izq == 323 && top == 134))
            { gato = 3;
            }
            else if ((izq == 413 && top == 224))
            {
                gato = 4;
            }
            else if ((izq == 473 && top == 374))
            {
                gato = 5;
            }

            //todos de down
            else if ((izq == 143 && top == 194))
            {
                gato = 6;
            }
            else if ((izq == 263 && top == 254))
            {
                gato = 7;
            }
            else if ((izq == 323 && top == 74))
            {
                gato = 8;
            }
            else if ((izq == 413 && top == 164))
            {
                gato = 9;
            }
            else if ((izq == 473 && top == 284))
            {
                gato = 10;
            }



                return gato;


        }
    }
}
