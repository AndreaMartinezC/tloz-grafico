﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TLOZ
{
    public partial class Form1 : Form
    {
       // Hero Link = new Hero();
        int puntaje = 0;
        
        
        public Form1()
        {
            InitializeComponent();
            PBSword.Location = new Point(83, 224);
            
        }


        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            //string paco = PBSword.Visible.ToString();
            
            label3.Visible = false;
            int izq = (int)PBLink.Left;
            int top = (int)PBLink.Top;
            int X = Hero.Mover(izq);
            int Y = Hero.MoverY(top);
            int ObsX = Obstaculo.BloquearX(izq, top);
            int ObsY = Obstaculo.BloquearY(izq, top);
            
            string Ene1visible = PBEnemigo1.Visible.ToString();
            string Ene2visible = PBEnemigo2.Visible.ToString();
            int EneX = Enemigos.EnemigoX(izq, top, Ene1visible, Ene2visible );
            int EneY=Enemigos.EnemigoY(izq, top, Ene1visible, Ene2visible);

            string LinkPos = PBLink.Location.ToString();
            string SwordPos = PBSword.Location.ToString();
            string Swordvis = PBSword.Visible.ToString();

            string ataque = Hero.Atacar(LinkPos, Swordvis);

            int arma = Arma.Recoger(LinkPos, SwordPos, Swordvis);

            //////////////////////////////////////////////////////////////////////////////////////////////////////

            if (e.KeyCode == Keys.Left)
            {
                if (X == 0)
                {
                    PBLink.Location = new Point(PBLink.Left, PBLink.Top);
                    label3.Visible = true;

                }

                else if (ObsX == 1 || ObsX == 3 || ObsX == 5 || ObsX == 7 || ObsX == 9)
                {
                    PBLink.Location = new Point(PBLink.Left, PBLink.Top);
                }

                //enemigos
                else if (EneX==1 || EneX==2)
                {
                    MessageBox.Show("El enemigo te ha atacado! Vuelves a comenzar");
                    puntaje = 0;
                    PBLink.Location = new Point(23, 224);
                    PBSword.Visible = true;
                    PBEnemigo1.Visible = true;
                    PBEnemigo2.Visible = true;

                }
                

                else PBLink.Left -= 30;
            }

            
            
            ///////////////////////////////////////////////////////////////////////////////////////////////////
            if (e.KeyCode == Keys.Right)
            {
                if (X == 1)
                {
                    PBLink.Location = new Point(PBLink.Left, PBLink.Top);
                    label3.Visible = true;
                }
                
                else if (ObsX == 2 || ObsX == 4 || ObsX == 6 || ObsX == 8 || ObsX == 10)
                {
                    PBLink.Location = new Point(PBLink.Left, PBLink.Top);

                }
                //enemigos
                else if (EneX==3 || EneX==4)
                {
                    MessageBox.Show("El enemigo te ha atacado! Vuelves a comenzar");
                    puntaje = 0;
                    PBLink.Location = new Point(23, 224);
                    PBSword.Visible = true;
                    PBEnemigo1.Visible = true;
                    PBEnemigo2.Visible = true;

                }
               
                else PBLink.Left += 30;

            }
             /////////////////////////////////////////////////////////////////////////////////////////////////////   
            if (e.KeyCode == Keys.Up)
            {
                if (Y==0)
                {
                    PBLink.Location = new Point(PBLink.Left, PBLink.Top);
                    label3.Visible = true;
                }
                else if (ObsY==1 || ObsY == 2 || ObsY == 3 || ObsY == 4 || ObsY == 5)
                {
                    PBLink.Location = new Point(PBLink.Left, PBLink.Top);

                }
             
                //enemigos
                else if (EneY==1 || EneY==2)
                {
                    MessageBox.Show("El enemigo te ha atacado! Vuelves a comenzar");
                    puntaje = 0;
                    PBLink.Location = new Point(23, 224);
                    PBSword.Visible = true;
                    PBEnemigo1.Visible = true;
                    PBEnemigo2.Visible = true;

                }
               
                else PBLink.Top -= 30;
            }
            ///////////////////////////////////////////////////////////////////////////////////////////////////////
            if (e.KeyCode == Keys.Down)
            {
                if (Y==1)
                {
                    PBLink.Location = new Point(PBLink.Left, PBLink.Top);
                    label3.Visible = true;
                }
                else if (ObsY ==6 || ObsY == 7 || ObsY == 8 || ObsY == 9 || ObsY == 10)
                {
                    PBLink.Location = new Point(PBLink.Left, PBLink.Top);

                }
                
                //enemigos
                else if (EneY==3 || EneY==4)
                {
                    MessageBox.Show("El enemigo te ha atacado! Vuelves a comenzar");
                    puntaje = 0;
                    PBLink.Location = new Point(23, 224);
                    PBSword.Visible = true;
                    PBEnemigo1.Visible = true;
                    PBEnemigo2.Visible = true;

                }
               
                else PBLink.Top += 30;
            }
            ////////////////////////////////////////////////////////////////////////////////////////////////////////
            if(arma==1)
            {
                PBSword.Visible = false;
                MessageBox.Show("Has obtenido la espada!");
               
                
            }
            ///////////////////////////////////////////////////////////////////////////////////////////////////////
            if(e.KeyCode==Keys.X)
            {


                if (ataque=="1")
                {
                    PBEnemigo1.Visible = false;
                    puntaje += 20;
                }
                
                else if (ataque=="2")
                {
                  

                    PBEnemigo2.Visible = false;
                    puntaje += 10;
                }
                else MessageBox.Show("No tienes arma, ve a buscar una");

            }
            
            marcador.Text = puntaje.ToString();

        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            label1.Text =PBLink.Location.ToString();
            

        }
    }
}
