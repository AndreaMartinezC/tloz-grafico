﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TLOZ
{
    class Enemigos
    {
        public static int EnemigoX(int izq, int top, string Ene1visible, string Ene2visible)
        {
            int mesa = 0;
            if (((izq == 233 && top == 104) || (izq == 233 && top == 74)) && Ene1visible == "True")
            {
                mesa = 1;
            }
            else if ((izq == 473 && top == 284) && Ene2visible == "True")
            {
                mesa = 2;
            }

            else if (((izq == 173 && top == 104) || (izq == 173 && top == 74)) && Ene1visible == "True")
            {
                mesa = 3;
            }
            else if ((izq == 383 && top == 284) && Ene2visible == "True")
            {
                mesa = 4;
            }
            
                return mesa;
        }

        public static int EnemigoY(int izq, int top, string Ene1visible, string Ene2visible)
        {
            int silla = 0;
            if ((izq == 203 && top == 134) && Ene1visible == "True")
            {
                silla = 1;
            }
            else if (((izq == 413 && top == 314) || (izq == 443 && top == 314)) && Ene2visible == "True")
            {
                silla = 2;
            }

            else if ((izq == 203 && top == 44) && Ene1visible == "True")
            {
                silla = 3;
            }
            else if (((izq == 413 && top == 254) || (izq == 443 && top == 254)) && Ene2visible == "True")
            {
                silla = 4;
            }
            return silla;

        }


    }
}
